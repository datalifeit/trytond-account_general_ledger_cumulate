datalife_account_general_ledger_cumulate
========================================

The account_general_ledger_cumulate module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_general_ledger_cumulate/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_general_ledger_cumulate)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
