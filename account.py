# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import datetime
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import If, Eval, Bool
from trytond.transaction import Transaction


class _GeneralLedgerAccount(object):
    __slots__ = ()

    @classmethod
    def get_period_ids(cls, name):
        pool = Pool()
        Period = pool.get('account.period')
        context = Transaction().context

        period = None
        if name.startswith('start_'):
            period_ids = []
            if context.get('start_period'):
                period = Period(context['start_period'])
        elif name.startswith('end_'):
            period_ids = []
            if context.get('end_period'):
                period = Period(context['end_period'])
            else:
                domain = [('type', '=', 'standard')]
                if context.get('fiscalyear'):
                    domain.append(
                        ('fiscalyear', '=', context.get('fiscalyear')))
                periods = Period.search(domain,
                    order=[('start_date', 'DESC')], limit=1)
                if periods:
                    period, = periods

        if period:
            domain = [('end_date', '<=', period.start_date)]
            if context.get('fiscalyear'):
                domain.append(
                    ('fiscalyear', '=', context.get('fiscalyear')))
            periods = Period.search(domain)
            if period.start_date == period.end_date:
                periods.append(period)
            if periods:
                period_ids = [p.id for p in periods]
            if name.startswith('end_'):
                # Always include ending period
                period_ids.append(period.id)

        return period_ids

    @classmethod
    def get_dates(cls, name):
        pool = Pool()
        Period = pool.get('account.period')

        from_date, to_date = super().get_dates(name)
        context = Transaction().context
        if name.startswith('start_') and not to_date:
            if not context.get('from_date'):
                # get first date of fiscal year
                period_domain = [('company', '=', context['company'])]
                if context.get('fiscalyear'):
                    period_domain.append(
                        ('fiscalyear', '=', context['fiscalyear']))
                periods = Period.search(period_domain,
                    order=[('start_date', 'ASC')], limit=1)
                to_date = periods and periods[0].start_date
            else:
                to_date = context.get('from_date')

            if to_date:
                to_date -= datetime.timedelta(days=1)
        return from_date, to_date

    @classmethod
    def get_account(cls, records, name):
        Account = cls._get_account()

        period_ids, from_date, to_date = None, None, None
        context = Transaction().context
        if context.get('start_period') or context.get('end_period'):
            period_ids = cls.get_period_ids(name)
        # FIX core bug that use "end_date" instead of "to_date"
        elif context.get('from_date') or context.get('to_date'):
            from_date, to_date = cls.get_dates(name)
        else:
            if name.startswith('start_'):
                period_ids = []

        with Transaction().set_context(
                periods=period_ids,
                from_date=from_date, to_date=to_date):
            accounts = Account.browse(records)
        fname = name
        for test in ['start_', 'end_']:
            if name.startswith(test):
                fname = name[len(test):]
                break
        return {a.id: getattr(a, fname) for a in accounts}

    @classmethod
    def table_query(cls):
        with Transaction().set_context(closed_fiscalyears=True):
            return super().table_query()


class GeneralLedgerAccount(_GeneralLedgerAccount, metaclass=PoolMeta):
    __name__ = 'account.general_ledger.account'


class GeneralLedgerAccountParty(_GeneralLedgerAccount, metaclass=PoolMeta):
    __name__ = 'account.general_ledger.account.party'


class GeneralLedgerAccountContext(metaclass=PoolMeta):
    __name__ = 'account.general_ledger.account.context'

    @classmethod
    def __setup__(cls):
        super(GeneralLedgerAccountContext, cls).__setup__()
        cls.fiscalyear.required = False
        for field_name in ('start_period', 'end_period'):
            _field = getattr(cls, field_name)
            domain = _field.domain[0]
            _field.domain[0] = If(
                Bool(Eval('fiscalyear')), domain, ())

    @classmethod
    def default_fiscalyear(cls):
        context = Transaction().context

        if not context.get('fiscalyear') and not context.get('period'):
            return None

        return super().default_fiscalyear()


class GeneralLedgerLineContext(metaclass=PoolMeta):
    __name__ = 'account.general_ledger.line.context'

    @classmethod
    def __setup__(cls):
        super(GeneralLedgerLineContext, cls).__setup__()
        cls.fiscalyear.required = False

    @classmethod
    def default_fiscalyear(cls):
        context = Transaction().context

        if not context.get('fiscalyear') and not context.get('period'):
            return None

        return super().default_fiscalyear()


class GeneralLedgerLine(metaclass=PoolMeta):
    __name__ = 'account.general_ledger.line'

    move_origin = fields.Reference("Move Origin", selection='get_move_origin')

    @classmethod
    def table_query(cls):
        with Transaction().set_context(closed_fiscalyears=True):
            query = super().table_query()
        move = query.from_[0].left.right
        columns = []
        for column in query.columns:
            if column.output_name == 'move_origin':
                column = move.origin.as_('move_origin')
            columns.append(column)
        query.columns = columns
        return query

    @classmethod
    def get_move_origin(cls):
        Move = Pool().get('account.move')
        return Move.get_origin()


class AgedBalance(metaclass=PoolMeta):
    __name__ = 'account.aged_balance'

    @classmethod
    def table_query(cls):
        with Transaction().set_context(closed_fiscalyears=True):
            return super().table_query()


class TrialBalance(metaclass=PoolMeta):
    __name__ = 'account.trial_balance'

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        Currency = pool.get('currency.currency')

        context = Transaction().context
        report_context = super().get_context(records, header, data)
        if not context.get('fiscalyear'):
            report_context['fiscalyear'] = None

        if not context.get('company'):
            report_context['company'] = None
            report_context['currency'], = Currency.search([
                ('code', '=', 'EUR')], limit=1)
        else:
            report_context['currency'] = report_context.get('company').currency

        return report_context
