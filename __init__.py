# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import account
from . import move


def register():
    Pool.register(
        account.GeneralLedgerAccount,
        account.GeneralLedgerAccountContext,
        account.GeneralLedgerLineContext,
        account.GeneralLedgerAccountParty,
        account.GeneralLedgerLine,
        account.AgedBalance,
        move.Line,
        module='account_general_ledger_cumulate', type_='model')
    Pool.register(
        account.TrialBalance,
        module='account_general_ledger_cumulate', type_='report')
